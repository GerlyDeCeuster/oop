﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ArraysEnKlassen
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. Prijzen met Foreach");
            Console.WriteLine("2. SPeelkaarten");
            Console.WriteLine("3. Organiseren van studenten");
            Console.WriteLine("4. Gemiddelde cijfers opvragen per vak");
            Console.WriteLine("5. Spelletje Hoger/Lager");
            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1: PrijzenMetForeach.AskForPrices();
                    break;
                case 2: PlayingCard.GenerateDeck();
                    break;
                case 3: Student.ExecuteStudentMenu();
                    break;
                case 4:Student.ExecuteStudentMenu();
                    break;
                case 5: PlayingCard.HigherLower();
                    break;
                default:
                    Console.WriteLine("geen geldige keuze!");
                    break;
            }
        }
    }
}
