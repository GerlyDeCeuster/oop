﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class PrijzenMetForeach
    {
        public static void AskForPrices()
        {
            double[] pricelist = new double[20];
            double total=0;
            double average=0;
            Console.WriteLine("Gelieve 20 prijzen in te geven.");
            for (int i = 0; i < 20;i++)
            {
                double price = Convert.ToDouble(Console.ReadLine());
                pricelist[i] = price;
            }

            foreach(double price in pricelist)
            {
                if(price>=5.00)
                {
                    Console.WriteLine(Math.Round(price,2));
                }
                total= total+price;
            }
            average = total / 20;
            Console.WriteLine($"Het gemiddelde bedrag is {Math.Round(average,2)}.");
        }
    }
}
