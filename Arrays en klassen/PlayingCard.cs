﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public enum Suites
    {
        Clubs,
        Hearts,
        Spades,
        Diamonds
    }

    class PlayingCard
    {
        private int number;
        public int Number
        {
            get { return number; }
            set { if(value>0 && value<14)
                    { number = value; }
                  else
                    { Console.WriteLine("ongeldige input"); }
                }
        }
        public Suites Suites { get; set; }

        static List<PlayingCard> Cards;

        public static void GenerateDeck()
        {
            Cards = new List<PlayingCard>();
            foreach (Suites suit in Enum.GetValues(typeof(Suites)))
            {
                for(int i=1;i<14;i++)
                {
                    PlayingCard Card = new PlayingCard();
                    Card.Number = i;
                    Card.Suites = suit;
                    Cards.Add(Card);
                }
            }
            //ShowShuffledDeck(Cards);
        }

        public static void ShowShuffledDeck(List<PlayingCard> cards)
        { 
            Random r = new Random();
            int p = 0;
            while(cards.Count>0)
            {
                p = r.Next(0, cards.Count);
                Console.WriteLine(cards[p].Number+" "+cards[p].Suites);
                cards.Remove(cards[p]);
            }
        }

        public static void HigherLower()
        {
            GenerateDeck();
            Random rand = new Random();
            bool error = false;
            PlayingCard previousCard=Cards[rand.Next(0, Cards.Count)];
            Cards.Remove(previousCard);
            Console.WriteLine("Klaar om te spelen.");
            do
            {
                Console.WriteLine("De vorige kaart had waarde " + previousCard.Number);
                PlayingCard currentCard = Cards[rand.Next(0, Cards.Count)];
                Cards.Remove(currentCard);
                Console.WriteLine("Hoger(0), lager(1) of gelijk(2) ?");
                int choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 0:
                        if (currentCard.Number<=previousCard.Number)
                        {
                            error = true;
                        }
                        break;
                    case 1:
                        if (currentCard.Number >= previousCard.Number)
                        {
                            error = true;
                        }
                        break;
                    case 2:
                        if (currentCard.Number != previousCard.Number)
                        {
                            error = true;
                        }
                        break;
                }
                previousCard = currentCard;
            }
            while (error == false && Cards.Count < 0);

            if (error==true || Cards.Count == 0)
                {
                     Console.WriteLine("Spel voorbij!");
                }
        }
    }
}
