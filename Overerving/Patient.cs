﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Patient
    {
        public string Name { get; set; }
        public double Verblijfsduur { get; set; }

        public Patient(string name, double verblijfsduur)
        {
            this.Name = name;
            this.Verblijfsduur = verblijfsduur;
        }

        public virtual void ShowCost(Patient patient)
        {
            double cost = 50 + (20 * Verblijfsduur);
            Console.WriteLine($"{patient.Name}, een gewone patiënt die {patient.Verblijfsduur} uur in het ziekenhuis gelegen heeft, betaald {cost}euro.");
        }

        public static void DemonstratePatients()
        {
            Patient patient1 = new Patient("Vincent", 12);
            patient1.ShowCost(patient1);
            InsuredPatient patient2 = new InsuredPatient("Tim", 12);
            patient2.ShowCost(patient2);
        }
    }
}
