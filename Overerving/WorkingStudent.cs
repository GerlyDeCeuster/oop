﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class WorkingStudent: Student
    {
        private int workHours= 10;
        public int WorkHours
        {
            get
            {
                return workHours;
            }

            set
            {
                if (value > 20)
                {
                    value = 20;
                }
                else if (value < 1)
                {
                    value = 1;
                }
                else
                {
                    value = workHours;
                }
            }
        }

        public bool HasWorkToday()
        {
            Random rnd = new Random();
            bool randomBool = rnd.Next(0, 2) > 0;
            return randomBool;
        }

        public override void ShowOverview()
        {
            base.ShowOverview();
            Console.WriteLine("Statuut: werkstudent");
            Console.WriteLine("Aantal werkuren per week:"+"\t" + WorkHours);
        }

        public static void DemonstrateWorkingStudent()
        {
            List<Student> Students = new List<Student>();
            Student student1 = new Student();
            student1.Classes = Class.EA2;
            student1.Age = 21;
            student1.Name = "Joske Vermeulen";
            student1.MarkCommunication = 12;
            student1.MarkProgrammingPrinciples = 15;
            student1.MarkWebTech = 13;
            

            WorkingStudent student2 = new WorkingStudent();
            student2.Classes = Class.EB1;
            student2.Age = 22;
            student2.Name = "Mieke Jansens";
            student2.MarkCommunication = 10;
            student2.MarkProgrammingPrinciples = 16;
            student2.MarkWebTech = 16;
            student2.WorkHours = 10;

            Students.Add(student1);
            Students.Add(student2);

            foreach (Student student in Students)
            {
                student.ShowOverview();
                Console.WriteLine();
            }
        }
    }
}
