﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class InsuredPatient: Patient
    {
        public InsuredPatient(string name, double verblijfsduur):base(name, verblijfsduur)
        {

        }
        public override void ShowCost(Patient insuredPatient)
        {
            double cost = ((( 50 +(20 * Verblijfsduur))/100)*90);
            Console.WriteLine($"{insuredPatient.Name}, een verzekerde patiënt die {insuredPatient.Verblijfsduur} uur in het ziekenhuis gelegen heeft, betaald {cost} euro.");
        }
    }
}
