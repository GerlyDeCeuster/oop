﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class RecurringTask:Task
    {
        public RecurringTask(string description, byte aantalDagen): base(description)
        {
            Console.WriteLine($"Deze taak moet om de {aantalDagen} dagen herhaald worden.");
        }

        public static void DemonstrateTasks()
        {
            List<Task> Taken = new List<Task>();
            short choice;

            do
            {
                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("1. Een taak aanmaken");
                Console.WriteLine("2. Een terugkerende taak aanmaken");
                Console.WriteLine("3. Stoppen");
                choice = Convert.ToInt16(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Geef een beschrijving van de taak.");
                        string beschrijving = Console.ReadLine();
                        Task Task1 = new Task(beschrijving);
                        Taken.Add(Task1);
                        break;
                    case 2:
                        Console.WriteLine("Geef een beschrijving van de terugkerende taak.");
                        string beschrijving2 = Console.ReadLine();
                        Console.WriteLine("Om de hoeveel dagen moet de taak terugkeren?");
                        byte days = byte.Parse(Console.ReadLine());
                        RecurringTask Task2 = new RecurringTask(beschrijving2, days);
                        Taken.Add(Task2);
                        break;
                    case 3:
                        break;
                }
            }
            while (choice != 3);
        }
    }
}
