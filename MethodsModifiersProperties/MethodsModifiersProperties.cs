﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class MethodsModifiersProperties
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. Uitslagen afdrukken.");
            Console.WriteLine("2. Uitslagen afdrukken versie 2.");
            Console.WriteLine("3. Getallencombinatie");
            Console.WriteLine("4. Figuren");
            Console.WriteLine("5. Studentklasse");
            Console.WriteLine("6. RapportModule-V3");
            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    ResultV1.Main();
                    break;
                case 2:
                    ResultV2.Main();
                    break;
                case 3:
                    NumberCombination.Main();
                    break;
                case 4:
                    FigureProgram.Main();
                    break;
                case 5:
                    Student.Main();
                    break;
                case 6:
                    RapportModule_V3.Main();
                    break;
                default:
                    Console.WriteLine("geen geldige keuze!");
                    break;
            }
        }
    }
}
