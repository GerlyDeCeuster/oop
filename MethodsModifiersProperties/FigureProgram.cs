﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class FigureProgram
    {
        public static void Main ()
        {
            Rectangle rectangle1 = new Rectangle(2.2,1.5);
            Rectangle rectangle2 = new Rectangle(3, 1);
            Triangle triangle1 = new Triangle(3, 1);
            Triangle triangle2 = new Triangle(2, 2);          

            Console.WriteLine($"Een rechthoek met een breedte van 2.2m en een hoogte van 1,5 m heeft een oppervlakte van {rectangle1.Surface}m².");
            Console.WriteLine($"Een rechthoek met een breedte van 3m en een hoogte van 1 m heeft een oppervlakte van {rectangle2.Surface}m².");
            Console.WriteLine($"Een driehoek met een basis van 3m en een hoogte van 1 m heeft een oppervlakte van {triangle1.Surface}m².");
            Console.WriteLine($"Een driehoek met een basis van 2m en een hoogte van 2 m heeft een oppervlakte van {triangle2.Surface}m².");

        }
    }
}
