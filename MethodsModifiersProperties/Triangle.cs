﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Triangle
    {
        private double basis;
        public double Base
        {
            get { return basis; }
            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Base niet {value} zijn");
                }
                else
                { basis = value; }
            }
        }

        private double height;
        public double Height
        {
            get { return height; }
            set {
                if (value <= 0)
                {
                    Console.WriteLine($"Height mag niet {value} zijn");
                }
                else
                { height = value; }
            }
        }

        public Triangle (double basis, double height)
        {
                Base = basis;
                Height = height;
        }

        public double Surface
        {
            get
            {
                return (Base * Height) / 2;
            }
        }

    }
}
