﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Rectangle
    {
        private double width=1.0;
        public double Width
        {
            get
            {
                return width; 
            }

            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Width mag niet {value} zijn");
                }
                else
                { width = value; }
            }
        }

        private double height;
        public double Height
        {
            get
            {
                return height;
            }

            set
            {
                if (value <= 0)
                {
                    Console.WriteLine($"Height mag niet {value} zijn");
                }
                else
                { height = value; }
            }
        }

        public double Surface
        {
            get
            {
                return Width * Height;
            }
        }


        public Rectangle(double width, double height)
        { 
                Width = width;
                Height = height;
            
        }
    }
}
