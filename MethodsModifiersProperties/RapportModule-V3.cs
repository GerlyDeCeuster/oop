﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class RapportModule_V3
    {
        private byte percentage;
        public byte Percentage
        {
            get
            {
                if (percentage > 0 || percentage < 100)
                { return percentage; }
                else
                { throw new Exception("Getal moet tussen 0 en 100 liggen."); }
            }
            set
            {
                if (percentage > 0 || percentage < 100)
                {
                    percentage = value;
                }
                else
                { throw new Exception("Getal moet tussen 0 en 100 liggen."); }
            }
        }
        public Honors Honors
        {
            get
            {
                if (Percentage < 50)
                {
                    return Honors.Niet_geslaagd;
                }
                else if (Percentage > 50 && Percentage < 68)
                {
                    return Honors.Voldoende;
                }
                else if (Percentage > 68 && Percentage < 75)
                {
                    return Honors.Onderscheiding;
                }
                else if (Percentage > 75 && Percentage < 85)
                {
                    return Honors.Grote_onderscheiding;
                }
                else
                {
                    return Honors.Grootste_onderscheiding;
                }
            }
        }

        public static void Main()
        {
            RapportModule_V3 result1 = new RapportModule_V3();
            result1.Percentage = 40;
            Console.WriteLine(result1.Honors);

            RapportModule_V3 result2 = new RapportModule_V3();
            result2.Percentage = 60;
            Console.WriteLine(result2.Honors);

            RapportModule_V3 result3 = new RapportModule_V3();
            result3.Percentage = 80;
            Console.WriteLine(result3.Honors);

        }
        
    }  
}
