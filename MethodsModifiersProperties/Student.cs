﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    
    public enum Class
        {
            EA1,
            EA2,
            EB1
        }
    public class Student
    {
        public string Name { get; set; }
        private int age;
        public int Age
        { get
            { return age; }
          set
            {
                if (value >= 120)
                {
                    Console.WriteLine($"Student kan nooit {value} jaar zijn");
                }
                else
                { age = value; }
            }
        }
        public Class Classes { get; set; }
        private int markCommunication;
        public int MarkCommunication
        {
            get { return markCommunication; }
            set
            {
                if (value > 20)
                {
                    Console.WriteLine("Cijfer mag niet boven 20 zijn.");
                }
                else
                {
                    markCommunication= value;
                }
            }
        }
        private int markProgrammingPrinciples;
        public int MarkProgrammingPrinciples
        { get { return markProgrammingPrinciples; }
          set
            {
                if(value>20)
                {
                    Console.WriteLine("Cijfer mag niet boven 20 zijn.");
                }
                markProgrammingPrinciples = value;
            }
       }
        private int markWebTech;
        public int MarkWebTech
        {
            get { return markWebTech; }
            set
            {
                if (value > 20)
                {
                    Console.WriteLine("Cijfer mag niet boven 20 zijn.");
                }
                markWebTech = value;
            }
        }

        public double OverallMark
        {
            get { return (Convert.ToDouble(MarkCommunication) + MarkProgrammingPrinciples + MarkWebTech) / 3; }
        }

        public static List<Student> Students = new List<Student>();
        
        public virtual void ShowOverview ()
        {
            Console.WriteLine(Name+", "+Age+" jaar");
            Console.WriteLine("Klas: "+Classes);
            Console.WriteLine();
            Console.WriteLine("Cijferrapport:");
            Console.WriteLine("**********");
            Console.WriteLine("Communicatie:"+"\t"+MarkCommunication);
            Console.WriteLine("Programming Principles:" + "\t" + MarkProgrammingPrinciples);
            Console.WriteLine("Web Technology:" + "\t" + MarkWebTech);
            Console.WriteLine("Gemiddelde:" + "\t" + OverallMark);
        }

        public static void Main()
        {
            Student student1 = new Student();
            student1.Classes = Class.EA2;
            student1.Age = 21;
            student1.Name = "Joske Vermeulen";
            student1.MarkCommunication = 12;
            student1.MarkProgrammingPrinciples = 15;
            student1.MarkWebTech = 13;
            Students.Add(student1);

            Student student2 = new Student();
            student2.Classes = Class.EB1;
            student2.Age = 22;
            student2.Name = "Mieke Vermeulen";
            student2.MarkCommunication = 10;
            student2.MarkProgrammingPrinciples = 16;
            student2.MarkWebTech = 16;
            Students.Add(student2);
        }
        public static void AddStudent()
        {
            Student student = new Student();
            student.Classes = Class.EA1;
            student.Age = 18;
            student.Name = "Onbekend Onbekend";
            student.MarkCommunication = 10;
            student.MarkProgrammingPrinciples = 10;
            student.MarkWebTech = 10;
            Students.Add(student);
        }

        public static void AdjustStudent(Student student)
        {
            Console.WriteLine("Wat wil je aanpassen?");
            Console.WriteLine("1. Naam");
            Console.WriteLine("2. Leeftijd");
            Console.WriteLine("3. Cijfer communicatie");
            Console.WriteLine("4. Cijfer programmeren");
            Console.WriteLine("5. Cijfer webtechnologie");
            short input = Convert.ToInt16(Console.ReadLine());
            switch (input)
            {
                case 1:
                    Console.WriteLine("Wat is de nieuwe waarde?");
                    student.Name = Console.ReadLine();
                    break;
                case 2:
                    Console.WriteLine("Wat is de nieuwe waarde?");
                    student.Age = int.Parse(Console.ReadLine());
                    break;
                case 3:
                    Console.WriteLine("Wat is de nieuwe waarde?");
                    student.MarkCommunication = int.Parse(Console.ReadLine());
                    break;
                case 4:
                    Console.WriteLine("Wat is de nieuwe waarde?");
                    student.MarkProgrammingPrinciples = int.Parse(Console.ReadLine());
                    break;
                case 5:
                    Console.WriteLine("Wat is de nieuwe waarde?");
                    student.MarkWebTech = int.Parse(Console.ReadLine());
                    break;
            }
        }

        public static int AverageCommunication(List<Student> Students)
        {
            int total=0;
            foreach (var student in Students)
            {
                total += student.MarkCommunication;
            }
            int avg = total / Students.Count;
            return avg;
        }
        public static int AverageProgramming(List<Student> Students)
        {
            int total = 0;
            foreach (var student in Students)
            {
                total += student.MarkProgrammingPrinciples;
            }
            int avg = total / Students.Count;
            return avg;
        }
        public static int AverageWebtechnology(List<Student> Students)
        {
            int total = 0;
            foreach (var student in Students)
            {
                total += student.MarkWebTech;
            }
            int avg = total / Students.Count;
            return avg;
        }

        public static void ExecuteStudentMenu()
        {
            short choice;
            do
            {
                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("1. Gegevens van de studenten tonen");
                Console.WriteLine("2. Een nieuwe student toevoegen");
                Console.WriteLine("3. Gegevens van een bepaalde student aanpassen");
                Console.WriteLine("4. Een student uit het systeem verwijderen");
                Console.WriteLine("5. Toon het gemiddelde voor Communicatie");
                Console.WriteLine("6. Toon het gemiddelde voor Programmeren");
                Console.WriteLine("7. Toon het gemiddelde voor Webtechnologie");
                Console.WriteLine("8. Stoppen");

                choice = Convert.ToInt16(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        foreach(var student in Students)
                        {   student.ShowOverview();
                            Console.WriteLine();
                        }
                        break;
                    case 2:
                        AddStudent();
                        break;
                    case 3:
                        Console.WriteLine("Wat is de indexpositie van de student die je wil aanpassen?");
                        int input = Convert.ToInt16(Console.ReadLine());
                        AdjustStudent(Students[input]);
                        break;
                    case 4:
                        Console.WriteLine("Wat is de indexpositie van de te verwijderen student?");
                        int delete = Convert.ToInt16(Console.ReadLine());
                        Students.RemoveAt(delete);
                        break;
                    case 5:
                        Console.WriteLine("Gemiddelde cijfer op communicatie: "+ AverageProgramming(Students));
                        break;
                    case 6:
                        Console.WriteLine("Gemiddelde cijfer op programmeren: " + AverageWebtechnology(Students));
                        break;
                    case 7:
                        Console.WriteLine("Gemiddelde cijfer op webtechnologie: " + AverageCommunication(Students));
                        break;
                    case 8:
                        break;
                    default:
                        Console.WriteLine("geen geldige keuze!");
                        break;
                }
            }
            while (choice != 8);
        }

    }
}
