﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class ResultV1
    {
        public byte Percentage { get; set; }

        public static void Main()
        {

            ResultV1 result1 = new ResultV1();
            result1.Percentage = 40;
            result1.PrintHonors();

            ResultV1 result2 = new ResultV1();
            result2.Percentage = 60;
            result2.PrintHonors();
            ResultV1 result3 = new ResultV1();
            result3.Percentage = 80;
            result3.PrintHonors();
        }

        public void PrintHonors ()
        {
            if(Percentage<50)
            {
                Console.WriteLine("niet geslaagd");                              
            }
            else if (Percentage>50 || Percentage<68)
            {
                Console.WriteLine("voldoende");
            }
            else if (Percentage > 68 || Percentage < 75)
            {
                Console.WriteLine( "onderscheiding");
            }
            else if (Percentage > 75 || Percentage < 85)
            {
                Console.WriteLine("grote onderscheiding");
            }
            else
            {
                Console.WriteLine("grootste onderscheiding");
            }
        }
    }
}
