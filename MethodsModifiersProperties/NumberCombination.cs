﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class NumberCombination
    {
        private double number1;
        public double Number1
        {
            get
            {
                return number1;
            }

            set
            {
                number1 = value;
            }
        }

        private double number2;
        public double Number2
        {
            get
            {
                return number2;
            }

            set
            {
                number2 = value;
            }
        }

        public static void Main ()
        {
            NumberCombination pair1 = new NumberCombination();
            pair1.Number1 = 12;
            pair1.Number2 = 34;
            Console.WriteLine("Paar:" + pair1.Number1 + ", " + pair1.Number2);
            Console.WriteLine("Sum = " + pair1.Sum());
            Console.WriteLine("Verschil = " + pair1.Difference());
            Console.WriteLine("Product = " + pair1.Product());
            Console.WriteLine("Quotient = " + pair1.Quotient());

        }

        public double Sum()
        {
            return Number1 + Number2;
        }

        public double Difference ()
        {
            return Number1 - Number2;
        }

        public double Product()
        {
            return Number1 * Number2;
        }

        public double Quotient()
        {
            if(Number2==0)
            {
                Console.WriteLine("Error");
            }
            return Number1 / Number2;
        }
    }
}
