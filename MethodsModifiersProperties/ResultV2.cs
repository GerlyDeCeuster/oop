﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public enum Honors
    {
        Niet_geslaagd,
        Voldoende,
        Onderscheiding,
        Grote_onderscheiding,
        Grootste_onderscheiding
    }
    public class ResultV2
    {
        public byte Percentage { get; set; }

        public static void Main ()
        {
            ResultV2 result1 = new ResultV2();
            result1.Percentage = 40;
            Console.WriteLine(result1.ComputeHonors());

            ResultV2 result2 = new ResultV2();
            result2.Percentage = 60;
            Console.WriteLine(result2.ComputeHonors()); 

            ResultV2 result3 = new ResultV2();
            result3.Percentage = 80;
            Console.WriteLine(result3.ComputeHonors());

        }

        public Honors ComputeHonors()
        {
            if (Percentage < 50)
            {
               return Honors.Niet_geslaagd;
            }
            else if (Percentage > 50 && Percentage < 68)
            {
                return Honors.Voldoende;
            }
            else if (Percentage > 68 && Percentage < 75)
            {
                return Honors.Onderscheiding;
            }
            else if (Percentage > 75 && Percentage < 85)
            {
                return Honors.Grote_onderscheiding;
            }
            else
            {
                return Honors.Grootste_onderscheiding;
            }
        }
    }
}
