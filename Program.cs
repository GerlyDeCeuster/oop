﻿using System;
using OOP.Geometry;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice;
            while (true)
            {
                Console.WriteLine("Welk onderwerp wil je testen?");
                Console.WriteLine("1. Klassen en objecten");
                Console.WriteLine("2. Leren werken met DateTime");
                Console.WriteLine("3. Methods Modifiers Properties");
                Console.WriteLine("4. Geheugenmanagement bij klassen");
                Console.WriteLine("5. Geavanceerde klassen en objecten");
                Console.WriteLine("6. Arrays en klassen");
                Console.WriteLine("7. Overerving");

                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        KlassenEnObjecten.StartSubmenu();
                        break;
                    case 2:
                        LerenWerkenMetDateTime.StartSubmenu();
                        break;
                    case 3:
                        MethodsModifiersProperties.StartSubmenu();
                        break;
                    case 4:
                        GeheugenmanagementBijKlassen.StartSubmenu();
                        break;
                    case 5:
                        GeavanceerdeKlassenEnObjecten.StartSubmenu();
                        break;
                    case 6:
                        ArraysEnKlassen.StartSubmenu();
                        break;
                    case 7:
                        Overerving.StartSubmenu();
                        break;
                    default:
                        Console.WriteLine("Ongeldige keuze!");
                        break;
                }
            }
        }
    }
}
