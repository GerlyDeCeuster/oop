﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class LabCourse: Course
    {
        public byte StudyPoints { get; set; }
        public string Materials { get; set; }

        public LabCourse(string title, byte studyPoints, string materials) : base(title)
        {
            this.StudyPoints = studyPoints;
            this.Materials = materials;
        }
        public override uint CalculateWorkLoad()
        {
            return this.StudyPoints * (uint)2;
        }
    }
}
