﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schoolbeheer");
            School school1 = new School("GO! BS de Spits","Thonetlaan 106","2050","Antwerpen",1);
            School school2 = new School("GO! Koninklijk Atheneum Deurne","Fr. Craeybeckxlaan 22","2100","Deurne",2);
            School.List= new List<School>();
            School.List.Add(school1);
            School.List.Add(school2);
            Console.WriteLine(School.ShowAll());

            Student student1 = new Student("Mohamed","El Farisi",new DateTime (1987,12,06),1,1,"999");
            Student student2 = new Student("Sarah","Jansens",new DateTime(1991,10,21),2,2, "998");
            Student student3 = new Student("Bart","Jansens",new DateTime(1991,10,21),3,2, "997");
            Student student4 = new Student("Farah", "El Farisi", new DateTime(1987, 12, 06), 4, 1, "996");
            Student.List = new List<Student>();
            Student.List.Add(student1);
            Student.List.Add(student2);
            Student.List.Add(student3);
            Student.List.Add(student4);
            Console.WriteLine(Student.ShowAll());

            Lecturer lecturer1 = new Lecturer("Adem","Kaya",new DateTime(1976, 12, 01),1,1, "995");
            Lecturer lecturer2 = new Lecturer("Anne","Wouters",new DateTime(1968, 04, 03),2,2, "9994");
            Lecturer.List=new List<Lecturer>();
            Lecturer.List.Add(lecturer1);
            Lecturer.List.Add(lecturer2);
            Console.WriteLine(Lecturer.ShowAll());

            AdministrativeStaff staff1 = new AdministrativeStaff("Raul","Jacob",new DateTime(1985, 11, 01),1,1, "993");
            AdministrativeStaff.List = new List<AdministrativeStaff>();
            AdministrativeStaff.List.Add(staff1);
            Console.WriteLine(staff1.GetNameTagText());
            Console.WriteLine(staff1.ShowOne());

            var persons = new List<Person>();
            persons.Add(lecturer1);
            persons.Add(student1);
            persons.Add(staff1);
            foreach (var person in persons)
            {
                Console.WriteLine(person.GetNameTagText());
            }

            Course theoryCourse = new TheoryCourse("OO Programmeren", 3);
            Course seminar = new Seminar("Docker");
            lecturer1.Courses.Add(theoryCourse);
            lecturer1.Courses.Add(seminar);
            Console.WriteLine($"Cursussen van Adem: {lecturer1.ShowTaughtCourses()}");
            Console.ReadLine();
        }
    }
}
