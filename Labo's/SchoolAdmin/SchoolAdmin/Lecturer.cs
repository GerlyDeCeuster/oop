﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Lecturer: Person
    {
            
            public static List<Lecturer> List { get; set; }

            public List <Course> Courses { get; set; }

            public Lecturer (string firstName, string lastName, DateTime birthDay, int id, int schoolId, string contactNumber) : base(firstName, lastName, birthDay, id, schoolId, contactNumber)
            {
            this.Courses = new List<Course>();
            }

            public static string ShowAll()
            {
                string text = "Lijst van lectoren:\n";
                foreach (var lector in Lecturer.List)
                {
                    text += $"{lector.FirstName}, {lector.LastName}, {lector.Birthday.Date}, {lector.Id}, {lector.SchoolId}\n";
                }
                return text;
            }

            public override string ShowOne()
            {
                return $"Gegevens van de lector: {this.FirstName}, {this.LastName}, {this.Birthday}, {this.Id}, {this.SchoolId}";
            }
            
            public string ShowTaughtCourses()
            {
                string result = "Vakken van deze lector:";
                foreach (var course in Courses)
                {
                    result += $"{course.Title}({course.CalculateWorkLoad()})\n";
                }
                return result;
            }

            public override  string GetNameTagText()
            {
                return $"(LECTOR) {this.FirstName} {this.LastName} \n {this.ContacNumber}";
            }
    }
}

