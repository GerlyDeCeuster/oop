﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Seminar: Course
    {
        
        public Seminar (string title) : base(title)
        {
            
        }
        public override uint CalculateWorkLoad()
        {
            return 20;
        }
    }
}
