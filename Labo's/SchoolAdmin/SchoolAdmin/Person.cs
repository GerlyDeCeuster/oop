﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    abstract class Person
    {
        public string FirstName { get; set; }
        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set
            {
                if (string.IsNullOrEmpty(lastName))
                { lastName = value; }
            }
        }
        protected DateTime Birthday { get; set; }
        public int Id { get; set; }
        public int SchoolId { get; set; }
        protected string ContacNumber { get; set; }

        public Person (string firstName, string lastName, DateTime birthDay, int id, int schoolId, string contactNumber)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Birthday = birthDay;
            this.Id = id;
            this.SchoolId = schoolId;
            this.ContacNumber = contactNumber;
        }

        public abstract string ShowOne();
        
        public virtual string GetNameTagText()
        {
            return $"{this.FirstName} {this.LastName}";
        }
    }
}
