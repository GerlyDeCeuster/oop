﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class AdministrativeStaff: Person
    {
        public static List<AdministrativeStaff> List { get; set; }
        public AdministrativeStaff(string firstName, string lastName, DateTime birthDay, int id, int schoolId, string contactNumber) : base(firstName, lastName, birthDay, id, schoolId, contactNumber)
        {

        }
        public static string ShowAll()
        {
            string text = "Lijst van administratieve medewerkers:\n";
            foreach (var staff in List)
            {
                text += $"{staff.FirstName}, {staff.LastName}, {staff.Birthday.Date}, {staff.Id}, {staff.SchoolId}\n";
            }
            return text;
        }
        public override string ShowOne()
        {
            return $"Gegevens van de administratieve medewerker: {this.FirstName}, {this.LastName}, {this.Id}, {this.SchoolId}";
        }
        public override string GetNameTagText()
        {
            return $"(ADMINISTRATIE) {this.FirstName} {this.LastName} \n {this.ContacNumber}";
        }
    }
}
