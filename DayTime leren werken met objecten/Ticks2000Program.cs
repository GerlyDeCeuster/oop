﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Ticks2000Program
    {
        public static void Main()
        {
            DateTime currentTime = DateTime.Now;
            DateTime centuryBegin = new DateTime(2000, 1, 1);

            long elapsedTicks = currentTime.Ticks - centuryBegin.Ticks;
            Console.WriteLine($"Sinds {centuryBegin.ToString("dd MMMM yyyy")} zijn er {elapsedTicks} ticks voorbijgegaan.");

        }
    }
}
