﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace OOP
{
    public class DayOfWeekProgram
    {
        public static void Main()
        {
            Console.WriteLine("Welke dag?");
            int day = int.Parse(Console.ReadLine());
            Console.WriteLine("Welke maand?");
            int month = int.Parse(Console.ReadLine());
            Console.WriteLine("Welk jaar?");
            int year = int.Parse(Console.ReadLine());
            DateTime dayOfWeek = new DateTime(year, month, day);
            CultureInfo belgianIC = new CultureInfo("nl-BE");

            Console.WriteLine($"{dayOfWeek.ToString("dd MMMM yyyy", belgianIC)} is een {dayOfWeek.ToString("dddd")}.");
        }
    }
}
