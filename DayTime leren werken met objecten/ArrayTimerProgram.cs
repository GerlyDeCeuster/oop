﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class ArrayTimerProgram
    {
        public static void Main ()
        {
            int[] array= new int[1000000];
            DateTime start = DateTime.Now;
            for (int i=1;i<=array.Length;i++)
            {
                array[i] = i;

            }
            DateTime end = DateTime.Now;
            long milli = end.Millisecond - start.Millisecond;
            Console.WriteLine($"Het duurt {milli} milliseconden om een array van een miljoen elementen aan te maken en op te vullen met opeenvolgende waarden.");
        }
    }
}
