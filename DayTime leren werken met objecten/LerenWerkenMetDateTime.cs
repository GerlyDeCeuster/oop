﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class LerenWerkenMetDateTime
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. Dag van de week");
            Console.WriteLine("2. Aantal ticks sinds 2000");
            Console.WriteLine("3. Aantal schrikkeljaren tussen 1800 en 2020");
            Console.WriteLine("4. Aantal milliseconden in array van miljoen");
            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    DayOfWeekProgram.Main();
                    break;
                case 2:
                    Ticks2000Program.Main();
                    break;
                case 3:
                    LeapYearProgram.Main();
                    break;
                case 4:
                    ArrayTimerProgram.Main();
                    break;
                default:
                    Console.WriteLine("geen geldige keuze!");
                    break;
            }
        }
    }
}
