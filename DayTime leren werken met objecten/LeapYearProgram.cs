﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class LeapYearProgram
    {
        public static void Main ()
        {
            int count = 0;
            for (int year = 1800; year <= 2020; year++)
            {
                if (DateTime.IsLeapYear(year))
                {
                    count++;
                }
            }
            Console.WriteLine($"Er zijn {count} schrikkeljaren tussen 1800 en 2020.");
        }
    }
}
