﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Pokebattlecount
    {
        public static void DemonstrateCounter()
        {
            int GrassCounter = 0;
            int FireCounter = 0;
            int ElectricCounter = 0;
            int WaterCounter = 0;

            Pokemon[] pokemons = new Pokemon[4];
            Pokemon[] listPokemons = new Pokemon[5];
            Random rand = new Random();
            Pokemon bulbasaur = new Pokemon(PokeSpecies.Bulbasaur, PokeTypes.Grass, 20, 20);
            pokemons[0] = bulbasaur;
            Pokemon charmander = new Pokemon(PokeSpecies.Charmander, PokeTypes.Fire, 20, 20);
            pokemons[1] = charmander;
            Pokemon squirtle = new Pokemon(PokeSpecies.Squirtle, PokeTypes.Water, 20, 20);
            pokemons[2] = squirtle;
            Pokemon pikachu = new Pokemon(PokeSpecies.Pikachu, PokeTypes.Electric, 20, 20);
            pokemons[3] = pikachu;

            for (int i=0;i<listPokemons.Length;i++)
            {
                listPokemons[i] = pokemons[rand.Next(0, 4)];
                for(int j=0;j<rand.Next(5,10);j++)
                {
                    Pokemon.Attack(listPokemons[i]);
                    if(listPokemons[i].PokeType==PokeTypes.Grass)
                    {
                        GrassCounter++;
                    }
                    else if (listPokemons[i].PokeType == PokeTypes.Fire)
                    {
                        FireCounter++;
                    }
                    else if (listPokemons[i].PokeType == PokeTypes.Electric)
                    {
                        ElectricCounter++;
                    }
                    else if (listPokemons[i].PokeType == PokeTypes.Water)
                    {
                        WaterCounter++;
                    }

                }
            }
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Grass`: {GrassCounter}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Fire`: {FireCounter}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Electric`: {ElectricCounter}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Water`: {WaterCounter}");
        }
    }
}
