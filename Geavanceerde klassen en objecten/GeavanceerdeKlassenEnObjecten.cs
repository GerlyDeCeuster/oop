﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class GeavanceerdeKlassenEnObjecten
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. Pokemon Battle Count");
            Console.WriteLine("2. Tombola");
            Console.WriteLine("3. CSV Demo");
            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Pokebattlecount.DemonstrateCounter();
                    break;
                case 2:
                    Ticket.Raffle();
                    break;
                case 3:
                    CSVDemo.Run();
                    break;
                default:
                    Console.WriteLine("geen geldige keuze!");
                    break;
            }
        }
    }
}
