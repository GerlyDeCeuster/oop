﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Ticket
    {
        public byte Prize { get; set; }
        static Random rand = new Random();
        static byte[] bytes = new byte[100];
        public Ticket()
        {
            Prize = (byte)rand.Next(1,100);
        }

        public static void Raffle()
        {
            for(int i=0; i<10;i++)
            {
                Ticket ticket = new Ticket();
                Console.WriteLine($"Waarde van het lotje: {ticket.Prize}");
            }
        }
    }
}
