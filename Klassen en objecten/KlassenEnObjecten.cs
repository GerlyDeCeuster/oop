﻿using System;
using System.Collections.Generic;
using System.Text;
using OOP.Geometry;

namespace OOP
{
    class KlassenEnObjecten
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. Vormen tekenen (h8-vormen)");
            Console.WriteLine("2. Auto's laten rijden (h8-auto's)");
            Console.WriteLine("3. Patiënten tonen (h8-patiënten)");
            Console.WriteLine("4. Honden laten blaffen (h8-blaffende-honden)");
            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    ShapeBuilder shapes = new ShapeBuilder();
                    shapes.Color = ConsoleColor.Green;
                    shapes.Symbol = '*';
                    string line = shapes.Line(10);
                    Console.WriteLine(line);
                    string line2 = shapes.Line(10, 'p');
                    Console.WriteLine(line2);
                    shapes.Color = ConsoleColor.Red;
                    shapes.Symbol = '@';
                    string rectangle = shapes.Rectangle(10, 5);
                    Console.WriteLine(rectangle);
                    string rectangle2 = shapes.Rectangle(10, 5, '£');
                    Console.WriteLine(rectangle2);
                    shapes.Color = ConsoleColor.Blue;
                    shapes.Symbol = '#';
                    string triangle = shapes.Triangle(6);
                    Console.WriteLine(triangle);
                    string triangle2 = shapes.Triangle(4, '%');
                    Console.WriteLine(triangle2);
                    break;
                case 2:
                    Car car1 = new Car();
                    Car car2 = new Car();
                    for (int i = 1; i <= 5; i++)
                    {
                        car1.Gas();
                    }
                    for (int i = 1; i <= 3; i++)
                    {
                        car1.Brake();
                    }
                    for (int i = 1; i <= 10; i++)
                    {
                        car2.Gas();
                    }
                    car2.Brake();
                    Console.WriteLine($"Auto 1: {car1.Speed:f2} km/u, afgelegde weg: { car1.Odometer:f2}km");
                    Console.WriteLine($"Auto 2: { car2.Speed:f2}km/u, afgelegde weg: { car2.Odometer:f2}km");
                    break;
                case 3:
                    Console.WriteLine("Je hebt gekozen voor 3.");
                    break;
                case 4:
                    Console.WriteLine("Je hebt gekozen voor 4.");
                    break;
                default:
                    Console.WriteLine("geen geldige keuze!");
                    break;
            }
        }
    }
}
