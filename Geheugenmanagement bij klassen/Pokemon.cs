﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public enum PokeSpecies
    {
        Bulbasaur,
        Charmander,
        Squirtle,
        Pikachu
    }

    public enum PokeTypes
    {
        Grass,
        Fire,
        Water,
        Electric
    }

    public enum FightOutcome
    {
        WIN,
        LOSS,
        UNDECIDED
    }

    public class Pokemon
    {
        private int maxHP;
        public int MaxHP
        {
            get
            {
                { return maxHP; }
            }
            set
            {
                if (maxHP >= 20 && maxHP < 1000)
                {
                    maxHP = value;
                }
                else
                {
                    if (maxHP < 20)
                    {
                        maxHP = 20;
                    }
                    else
                    {
                        maxHP = 1000;
                    }
                }
            }
        }

        private int hp;
        public int HP
        {
            get
            {
                return hp;
            }
            set
            {
                if (value >= 0 || value <= maxHP)
                {
                    hp = value;
                }
                else if (value < 0)
                {
                    hp = 0;
                }
                else if (value > MaxHP)
                {
                    hp = MaxHP;
                }
            }
        }
        public PokeSpecies PokeSpecies { get; set; }
        public PokeTypes PokeType { get; set; }
        public FightOutcome Fightoutcome { get; set; }
        public Pokemon()
        { 
        }
        public Pokemon(PokeSpecies pokeSpecies, PokeTypes pokeTypes, int maxHP): this(pokeSpecies,pokeTypes,maxHP,0)
        {
            HP = maxHP / 2;
        }
        public Pokemon(PokeSpecies pokeSpecies,PokeTypes pokeTypes,int maxHP,int hp)
        {
            PokeSpecies = pokeSpecies;
            PokeType = pokeTypes;
            MaxHP = maxHP;
            HP = hp;
        }
       

        public static void MakePokemon()
        {
            Pokemon bulbasaur = new Pokemon(PokeSpecies.Bulbasaur,PokeTypes.Grass,20,20);
            Attack(bulbasaur);
            Pokemon charmander = new Pokemon(PokeSpecies.Charmander,PokeTypes.Fire,20,20);
            Attack(charmander);
            Pokemon squirtle = new Pokemon(PokeSpecies.Squirtle,PokeTypes.Water,20,20);
            Attack(squirtle);
            Pokemon pikachu = new Pokemon(PokeSpecies.Pikachu,PokeTypes.Electric,20,20);
            Attack(pikachu);
        }

        public static void Attack(Pokemon pokemon)
        {
            string pokespecies = pokemon.PokeSpecies.ToString();
            if (pokemon.PokeType.ToString()=="Grass")
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(pokespecies.ToUpper()+"!");
                Console.ResetColor();
            }
            else if (pokemon.PokeType.ToString() == "Fire")
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(pokespecies.ToUpper() + "!");
                Console.ResetColor();
            }
            else if (pokemon.PokeType.ToString() == "Water")
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(pokespecies.ToUpper() + "!");
                Console.ResetColor();
            }
            else if (pokemon.PokeType.ToString() == "Electric")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(pokespecies.ToUpper() + "!");
                Console.ResetColor();
            }
        }

        public static void FightOutcome(Pokemon pokemon1, Pokemon pokemon2, Random rand)
        {
            rand = new Random();
            int turn = rand.Next(1,2);

            if (pokemon1 != null && pokemon2 != null)
            {
                while (pokemon1.HP > 0 && pokemon2.HP > 0)
                {
                    if (turn % 2 == 0 && pokemon2.HP > 0)
                    {
                        Attack(pokemon2);
                        int damage = rand.Next(0, 20);
                        pokemon1.HP -= damage;
                        Console.WriteLine(pokemon2.PokeSpecies + " attacked " + pokemon1.PokeSpecies + " for " + damage + " damage'");
                        turn++;
                    }
                    else if (turn % 2 == 1 && pokemon1.HP > 0)
                    {
                        Attack(pokemon1);
                        int damage = rand.Next(0, 20);
                        pokemon2.HP -= damage;
                        Console.WriteLine(pokemon1.PokeSpecies + " attacked " + pokemon2.PokeSpecies + " for " + damage + " damage'");
                        turn++;
                    }
                }
            }
            if(pokemon1 is null && pokemon2 is null || pokemon1 is null && pokemon2.HP<=0 || pokemon2 is null && pokemon1.HP<=0)
            {
                Console.WriteLine("Beide pokemons zijn verloren!");
            }
            else if(pokemon1 is null)
            {
                Console.WriteLine(pokemon2.PokeSpecies + " is gewonnen!");
            }
            else if(pokemon2 is null)
            {
                Console.WriteLine(pokemon1.PokeSpecies + " is gewonnen!");
            }
            else if (pokemon1.HP<=0)
            {
                pokemon1.Fightoutcome = OOP.FightOutcome.LOSS;
                Console.WriteLine(pokemon2.PokeSpecies+" is gewonnen!");
            }
            else if(pokemon2.HP<=0)
            {
                pokemon1.Fightoutcome = OOP.FightOutcome.WIN;
                Console.WriteLine(pokemon1.PokeSpecies+" is gewonnen!");
            }
            else if (pokemon1.HP<=0 && pokemon2.HP<=0)
            {
                pokemon1.Fightoutcome = OOP.FightOutcome.UNDECIDED;
                Console.WriteLine("Beide pokemons zijn verloren!");
            }
        }

        public static void DemoFightOutcome()
        {
            Pokemon bulbasaur = new Pokemon();
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            bulbasaur.MaxHP = 20;
            bulbasaur.HP = 0;
            Pokemon charmander = new Pokemon();
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            charmander.MaxHP = 20;
            charmander.HP = 0;
            Random rand = new Random();

            Pokemon pikachu=null;
            Pokemon squirtle = null;

            FightOutcome(pikachu, squirtle,rand);
        }

        public static void ConstructPokemonChained()
        {
            Pokemon squirtle = new Pokemon(PokeSpecies.Squirtle, PokeTypes.Water, 40);
            Console.WriteLine($"De nieuwe {squirtle.PokeSpecies} heeft maximum {squirtle.maxHP} HP en heeft momenteel {squirtle.HP} HP." );
        }
    }
}
