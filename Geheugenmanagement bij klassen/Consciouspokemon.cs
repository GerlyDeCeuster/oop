﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    public class Consciouspokemon
    {
        public static Pokemon FirstConciousPokemon(Pokemon[] pokemons)
        {

            for (int i = 0; i < pokemons.Length; i++)
            {
                if (pokemons[i].HP >= 1)
                {
                    return pokemons[i];
                }
            }
            return null;
        }

        public static void TestConsciousPokemon()
        {
            Pokemon[]pokemons = new Pokemon[4];

            Pokemon bulbasaur = new Pokemon();
            bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            bulbasaur.PokeType = PokeTypes.Grass;
            bulbasaur.MaxHP = 20;
            bulbasaur.HP = 0;
            pokemons[0] = bulbasaur;
            Pokemon charmander = new Pokemon();
            charmander.PokeSpecies = PokeSpecies.Charmander;
            charmander.PokeType = PokeTypes.Fire;
            charmander.MaxHP = 20;
            charmander.HP = 0;
            pokemons[1] = charmander;
            Pokemon squirtle = new Pokemon();
            squirtle.PokeSpecies = PokeSpecies.Squirtle;
            squirtle.PokeType = PokeTypes.Water;
            squirtle.MaxHP = 20;
            squirtle.HP = 2;
            pokemons[2] = squirtle;
            Pokemon pikachu = new Pokemon();
            pikachu.PokeSpecies = PokeSpecies.Pikachu;
            pikachu.PokeType = PokeTypes.Electric;
            pikachu.MaxHP = 20;
            pikachu.HP = 20;
            pokemons[3] = pikachu;

            Pokemon firstPokemon = FirstConciousPokemon(pokemons);
            Pokemon.Attack(firstPokemon);
        }
    }
}
