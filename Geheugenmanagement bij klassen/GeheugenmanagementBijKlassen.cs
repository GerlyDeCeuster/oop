﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class GeheugenmanagementBijKlassen
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. Pokemon aanmaken");
            Console.WriteLine("2. Eerste bewuste pokemon");
            Console.WriteLine("3. Bewuste pokemon verbeterd");
            Console.WriteLine("4. HP aanvullen");
            Console.WriteLine("5. Uitkomst gevecht");

            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Pokemon.MakePokemon();
                    break;
                case 2:
                    Consciouspokemon.TestConsciousPokemon();
                    break;
                case 3:
                    ConsciousPokemonImproved.TestConsciousPokemonSafe();
                    break;
                case 4:
                    Pokevalueref.DemoRestoreHP();
                    break;
                case 5:
                    Pokemon.DemoFightOutcome();
                    break;
                default:
                    Console.WriteLine("geen geldige keuze!");
                    break;
            }
        }
    }
}
